package ru.fedun.tm.command.project.show;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.ProjectDTO;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.Project;
import ru.fedun.tm.endpoint.SessionDTO;

import java.util.List;

public final class ProjectShowAllCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects list.";
    }

    @Override
    public void execute() throws Exception{
        System.out.println("[LIST PROJECTS]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        @NotNull final List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().showAllProjects(session);
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
