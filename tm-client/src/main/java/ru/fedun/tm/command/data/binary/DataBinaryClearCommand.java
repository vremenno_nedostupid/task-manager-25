package ru.fedun.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public final class DataBinaryClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-clear-bin";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY REMOVE]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().clearBinaryFile(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
