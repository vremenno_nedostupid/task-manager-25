package ru.fedun.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public final class DataBase64SaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-base64";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().saveBase64(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
