package ru.fedun.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER PROJECT TITLE]");
        @NotNull final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().createProject(session, title, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
