package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyTaskException extends AbstractRuntimeException {

    private final static String message = "Error! Task is empty...";
    public EmptyTaskException() {
        super(message);
    }

}
