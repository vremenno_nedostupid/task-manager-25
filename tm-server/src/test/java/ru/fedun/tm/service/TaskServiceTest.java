package ru.fedun.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.empty.EmptyDescriptionException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectIndexException;
import ru.fedun.tm.marker.UnitServerCategory;
import ru.fedun.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class TaskServiceTest {
/*

    private ITaskService taskService;

    @Before
    public void initService() {
        taskService = new TaskService(new TaskRepository());
    }

    @Test
    public void createTest() {
        taskService.create("001", "task");
        assertFalse(taskService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void createTestEmptyUserIdException() {
        taskService.create("", "task");
    }

    @Test(expected = EmptyTitleException.class)
    public void createTestEmptyNameException() {
        taskService.create("001", "");
    }

    @Test
    public void createWithDescriptionTest() {
        taskService.create("001", "task", "description");
        assertFalse(taskService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void createWithDescriptionTestEmptyUserIdException() {
        taskService.create("", "task", "description");
    }

    @Test(expected = EmptyTitleException.class)
    public void createWithDescriptionTestEmptyNameException() {
        taskService.create("001", "", "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void createWithDescriptionTestEmptyDescriptionException() {
        taskService.create("001", "task", "");
    }

    @Test
    public void addTest() {
        final Task task = new Task();
        taskService.add("001", task);
        assertFalse(taskService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void addTestEmptyUserIdException() {
        final Task task = new Task();
        taskService.add("", task);
    }

    @Test
    public void removeTest() {
        final Task task = new Task();
        taskService.add("001", task);
        assertFalse(taskService.getEntity().isEmpty());
        taskService.remove("001", task);
        assertTrue(taskService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeTestEmptyUserIdException() {
        final Task task = new Task();
        taskService.add("001", task);
        assertFalse(taskService.getEntity().isEmpty());
        taskService.remove("", task);
    }

    @Test
    public void getAllTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        assertNotNull(taskService.findAll("001"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getAllTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        List<Task> tasks = taskService.findAll("");
    }

    @Test
    public void clearTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.clear("001");
        assertTrue(taskService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void clearTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.clear("");
    }

    @Test
    public void getOneByIdTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        final String taskId = task1.getId();
        assertEquals(task1, taskService.getOneById("001", taskId));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getOneByIdTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        final String taskId = task1.getId();
        taskService.getOneById("", taskId);
    }

    @Test(expected = EmptyIdException.class)
    public void getOneByIdTestEmptyIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.getOneById("001", "");
    }

    @Test
    public void getOneByIndexTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        assertEquals(task1, taskService.getOneByIndex("001", 0));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getOneByIndexTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.getOneByIndex("", 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void getOneByIndexTestIncorrectIndexException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.getOneByIndex("001", -2);
    }

    @Test
    public void getOneByNameTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        task1.setTitle("task1");
        assertEquals(task1, taskService.getOneByTitle("001", "task1"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getOneByNameTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        task1.setTitle("task1");
        taskService.getOneByTitle("", "task1");
    }

    @Test(expected = EmptyTitleException.class)
    public void getOneByNameTestEmptyNameException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        task1.setTitle("task1");
        final Task task3 = taskService.getOneByTitle("001", "");
    }

    @Test
    public void removeOneByIdTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        final String taskId = task1.getId();
        taskService.removeOneById("001", taskId);
        assertFalse(taskService.getEntity().contains(task1));
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeOneByIdTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        final String taskId = task1.getId();
        taskService.removeOneById("", taskId);
    }

    @Test(expected = EmptyIdException.class)
    public void removeOneByIdTestEmptyIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.removeOneById("001", "");
    }

    @Test
    public void removeOneByIndexTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.removeOneByIndex("001", 0);
        assertFalse(taskService.getEntity().contains(task1));
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeOneByIndexTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.removeOneByIndex("", 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void removeOneByIndexTestIncorrectIndexException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        taskService.removeOneByIndex("001", -1);
    }

    @Test
    public void removeOneByNameTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        task1.setTitle("task1");
        taskService.removeOneByTitle("001", "task1");
        assertFalse(taskService.getEntity().contains(task1));
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeOneByNameTestEmptyUserIdException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        task1.setTitle("task1");
        taskService.removeOneByTitle("", "task1");
    }

    @Test(expected = EmptyTitleException.class)
    public void removeOneByNameTestEmptyNameException() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        task1.setTitle("task1");
        taskService.removeOneByTitle("001", "");
    }

    @Test
    public void updateTaskByIdTest() {
        final Task task = new Task();
        taskService.add("001", task);
        final String taskId = task.getId();
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        final String taskDescription = "newDescription";
        taskService.updateById("001", taskId, taskName, taskDescription);
        assertEquals(task.getTitle(), taskName);
        assertEquals(task.getDescription(), taskDescription);
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateTaskByIdTestEmptyUserIdException() {
        final Task task = new Task();
        taskService.add("001", task);
        final String taskId = task.getId();
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        final String taskDescription = "newDescription";
        taskService.updateById("", taskId, taskName, taskDescription);
    }

    @Test(expected = EmptyIdException.class)
    public void updateTaskByIdTestEmptyIdException() {
        final Task task = new Task();
        taskService.add("001", task);
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        final String taskDescription = "newDescription";
        taskService.updateById("001", "", taskName, taskDescription);
    }

    @Test(expected = EmptyTitleException.class)
    public void updateTaskByIdTestEmptyNameException() {
        final Task task = new Task();
        taskService.add("001", task);
        final String taskId = task.getId();
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskDescription = "newDescription";
        taskService.updateById("001", taskId, "", taskDescription);
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateTaskByIdTestEmptyDescriptionException() {
        final Task task = new Task();
        taskService.add("001", task);
        final String taskId = task.getId();
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        taskService.updateById("001", taskId, taskName, "");
    }

    @Test
    public void updateTaskByIndexTest() {
        final Task task = new Task();
        taskService.add("001", task);
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        final String taskDescription = "newDescription";
        taskService.updateByIndex("001", 0, taskName, taskDescription);
        assertEquals(task.getTitle(), taskName);
        assertEquals(task.getDescription(), taskDescription);
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateTaskByIndexTestEmptyUserIdException() {
        final Task task = new Task();
        taskService.add("001", task);
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        final String taskDescription = "newDescription";
        taskService.updateByIndex("", 0, taskName, taskDescription);
    }

    @Test(expected = IncorrectIndexException.class)
    public void updateTaskByIndexTestIncorrectIndexException() {
        final Task task = new Task();
        taskService.add("001", task);
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        final String taskDescription = "newDescription";
        taskService.updateByIndex("001", -2, taskName, taskDescription);
    }

    @Test(expected = EmptyTitleException.class)
    public void updateTaskByIndexTestEmptyNameException() {
        final Task task = new Task();
        taskService.add("001", task);
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskDescription = "newDescription";
        taskService.updateByIndex("001", 0, "", taskDescription);
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateTaskByIndexTestEmptyDescriptionException() {
        final Task task = new Task();
        taskService.add("001", task);
        task.setTitle("taskName");
        task.setDescription("description");
        final String taskName = "newTaskName";
        taskService.updateByIndex("001", 0, taskName, "");
    }

    @Test
    public void getListTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.add("001", task1);
        taskService.add("001", task2);
        assertNotNull(taskService.getEntity());
    }

    @Test
    public void loadVarargsTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.load(task1, task2);
        assertNotNull(taskService.getEntity());
    }

    @Test
    public void loadCollectionTest() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        tasks.add(task1);
        final Task task2 = new Task();
        tasks.add(task2);
        taskService.load(tasks);
        assertNotNull(taskService.getEntity());
    }

    @Test
    public void clearWithoutUserIdTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.load(task1, task2);
        taskService.clear();
        assertTrue(taskService.getEntity().isEmpty());
    }
*/

}
