package ru.fedun.tm.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.notfound.UserNotFoundException;
import ru.fedun.tm.marker.UnitServerCategory;
import ru.fedun.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@Category(UnitServerCategory.class)
public class UserRepositoryTest {
/*

    final UserRepository userRepository = new UserRepository();

    @Before
    public void init() {
        final User user1 = new User();
        user1.setLogin("user1");
        user1.setPasswordHash(HashUtil.salt("user1"));
        user1.setEmail("user1@mail.ru");
        user1.setFirstName("user1FirstName");
        user1.setSecondName("user1LastName");

        final User user2 = new User();
        user2.setLogin("user2");
        user2.setPasswordHash(HashUtil.salt("user2"));
        user2.setEmail("user2@mail.ru");
        user2.setFirstName("user2FirstName");
        user2.setSecondName("user2LastName");

        final User user3 = new User();
        user3.setLogin("user3");
        user3.setPasswordHash(HashUtil.salt("user3"));
        user3.setEmail("user3@mail.ru");
        user3.setFirstName("user3FirstName");
        user3.setSecondName("user3LastName");

        userRepository.add(user1, user2, user3);
    }

    @Test
    public void addTest() {
        userRepository.clear();
        final User user1 = new User();
        assertEquals(userRepository.getEntities().size(), 0);
        userRepository.add(user1);
        assertEquals(userRepository.getEntities().size(), 1);
    }

    @Test
    public void findByIdTest() {
        final User user = userRepository.getEntities().get(0);
        final String userId = user.getId();
        assertEquals(user, userRepository.findById(userId));
    }

    @Test
    public void findByLoginTest() {
        final User user = userRepository.getEntities().get(0);
        final String login = user.getLogin();
        assertEquals(user, userRepository.findByLogin(login));
    }

    @Test
    public void removeUserTest() {
        final User user = userRepository.getEntities().get(0);
        userRepository.remove(user);
        assertFalse(userRepository.getEntities().contains(user));
    }

    @Test
    public void removeByIdTest() {
        final User user = userRepository.getEntities().get(0);
        final String userId = user.getId();
        userRepository.removeById(userId);
        assertFalse(userRepository.getEntities().contains(user));
    }

    @Test
    public void removeByLoginTest() {
        final User user = userRepository.getEntities().get(0);
        final String login = user.getLogin();
        userRepository.removeByLogin(login);
        assertFalse(userRepository.getEntities().contains(user));
    }

    @Test
    public void clearRecordsTest() {
        assertEquals(userRepository.getEntities().size(), 3);
        userRepository.clear();
        assertEquals(userRepository.getEntities().size(), 0);
    }

    @Test
    public void addVarargsTest() {
        final UserRepository userRepository = new UserRepository();
        assertEquals(userRepository.getEntities().size(), 0);
        final User user1 = new User();
        final User user2 = new User();
        final User user3 = new User();
        userRepository.add(user1, user2, user3);
        assertEquals(userRepository.getEntities().size(), 3);
    }

    @Test
    public void addListTest() {
        final UserRepository userRepository = new UserRepository();
        assertEquals(userRepository.getEntities().size(), 0);
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        final User user3 = new User();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        userRepository.add(users);
        assertEquals(userRepository.getEntities().size(), 3);
    }

    @Test
    public void loadOneTest() {
        assertEquals(userRepository.getEntities().size(), 3);
        final User user = new User();
        userRepository.load(user);
        assertEquals(userRepository.getEntities().size(), 1);
    }

    @Test
    public void loadVarargsTest() {
        assertEquals(userRepository.getEntities().size(), 3);
        final User user1 = new User();
        final User user2 = new User();
        userRepository.load(user1, user2);
        assertEquals(userRepository.getEntities().size(), 2);
    }

    @Test
    public void loadListTest() {
        assertEquals(userRepository.getEntities().size(), 3);
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.load(users);
        assertEquals(userRepository.getEntities().size(), 2);
    }

    @Test(expected = UserNotFoundException.class)
    public void findByIdTestException() {
        userRepository.findById("fsd");
    }

    @Test(expected = UserNotFoundException.class)
    public void findByLoginTestException() {
        userRepository.findByLogin("fsd");
    }
*/

}
