package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyLoginException extends AbstractRuntimeException {

    private final static String message = "Error! Login is empty...";

    public EmptyLoginException() {
        super(message);
    }

}
