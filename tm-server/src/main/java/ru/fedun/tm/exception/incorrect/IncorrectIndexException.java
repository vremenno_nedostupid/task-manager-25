package ru.fedun.tm.exception.incorrect;

public final class IncorrectIndexException extends RuntimeException {

    public IncorrectIndexException(final String value) {
        super("Error! Index \"" + value + "\" is incorrect...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }
}
