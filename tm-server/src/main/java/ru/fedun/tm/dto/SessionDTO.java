package ru.fedun.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.entity.AbstractEntity;
import ru.fedun.tm.entity.Session;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SessionDTO extends AbstractEntity implements Cloneable {

    private Long startTime;

    private String userId;

    private String signature;

    public SessionDTO(@Nullable final Session session) {
        if (session == null) return;
        id = session.getId();
        startTime = session.getStartTime();
        signature = session.getSignature();
        userId = session.getUser().getId();
    }

    @NotNull
    public static SessionDTO toDTO(@NotNull final Session session) {
        return new SessionDTO(session);
    }

    @NotNull
    public static List<SessionDTO> toDTO(@NotNull final Collection<Session> sessions) {
        if (sessions.isEmpty()) return Collections.emptyList();
        @NotNull final List<SessionDTO> result = new ArrayList<>();
        for (@NotNull final Session session : sessions) {
            result.add(new SessionDTO(session));
        }
        return result;
    }

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
