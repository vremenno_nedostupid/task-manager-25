package ru.fedun.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM User e", Long.class).getSingleResult();
    }

    @Override
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User findOneByLogin(@NotNull final String login) {
        return entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM User e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByLogin(@NotNull final String login) {
        entityManager.createQuery("DELETE FROM User e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM User").executeUpdate();
    }

}
