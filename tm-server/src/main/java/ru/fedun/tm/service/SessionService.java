package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.api.repository.ISessionRepository;
import ru.fedun.tm.api.service.IPropertyService;
import ru.fedun.tm.api.service.ISessionService;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.dto.UserDTO;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.repository.SessionRepository;
import ru.fedun.tm.util.HashUtil;
import ru.fedun.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void persist(@NotNull Session session) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@NotNull Session session) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull Session session) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean checkDataAccess(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        @NotNull final UserDTO userDTO = UserDTO.toDTO(serviceLocator.getUserService().findOneByLogin(login));
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(userDTO.getPasswordHash());
    }

    @Override
    public boolean isValid(@NotNull final SessionDTO sessionDTO) {
        try {
            validate(sessionDTO);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@NotNull final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO.getSignature() == null) throw new AccessDeniedException();
        if (sessionDTO.getStartTime() == null) throw new AccessDeniedException();
        if (sessionDTO.getUserId() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionDTO.getSignature();
        @Nullable final String signatureTarget = createSignature(temp);
        final boolean checkSignature = signatureSource.equals(signatureTarget);
        if (!checkSignature) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        final boolean isContain = repository.contains(sessionDTO.getId());
        entityManager.close();
        if (!isContain) throw new AccessDeniedException();
    }

    @Override
    public void validate(@NotNull final SessionDTO sessionDTO, @NotNull final Role role) throws Exception {
        validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final UserDTO userDTO = UserDTO.toDTO(serviceLocator.getUserService().findOneById(userId));
        if (!role.equals(userDTO.getRole())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public SessionDTO open(@NotNull final String login, @NotNull final String password) throws Exception {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @NotNull final User user = serviceLocator.getUserService().findOneByLogin(login);
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setStartTime(System.currentTimeMillis());
        session.setSignature(createSignature(SessionDTO.toDTO(session)));
        persist(session);
        return SessionDTO.toDTO(session);
    }

    @Nullable
    @Override
    public String createSignature(@NotNull final SessionDTO sessionDTO) {
        sessionDTO.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        return SignatureUtil.sign(sessionDTO, salt, cycle);
    }

    @NotNull
    @Override
    public UserDTO getUser(@NotNull final SessionDTO sessionDTO) throws Exception {
        @NotNull final String userId = getUserId(sessionDTO);
        return UserDTO.toDTO(serviceLocator.getUserService().findOneById(userId));
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        return sessionDTO.getUserId();
    }

    @NotNull
    @Override
    public List<SessionDTO> getSessionList(@NotNull final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        @NotNull final List<SessionDTO> sessionsDTO = SessionDTO.toDTO(repository.findAllByUserId(sessionDTO.getUserId()));
        entityManager.close();
        return sessionsDTO;
    }

    @Override
    public void close(@NotNull final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(sessionDTO.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void closeAll(@NotNull final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByUserId(sessionDTO.getUserId());
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void signOutByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new AccessDeniedException();
        @NotNull final UserDTO userDTO = UserDTO.toDTO(serviceLocator.getUserService().findOneByLogin(login));
        @NotNull final String userId = userDTO.getId();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void signOutByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
