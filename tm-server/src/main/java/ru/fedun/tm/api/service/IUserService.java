package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.UserDTO;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User findOneById(@NotNull String id) throws Exception;

    @NotNull
    User findOneByLogin(@NotNull String login) throws Exception;

    @NotNull
    List<UserDTO> findAll();

    void removeById(@NotNull String id) throws Exception;

    void removeByLogin(@NotNull String login) throws Exception;

    void removeAll();

    void create(@NotNull String login, @NotNull String password) throws Exception;

    void create(
            @NotNull String login,
            @NotNull String password,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String email
    ) throws Exception;

    void create(
            @NotNull String login,
            @NotNull String password,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull Role role
    ) throws Exception;

    Long count();

    void updatePassword(@NotNull String userId, @NotNull String newPassword) throws Exception;

    void updateMail(@NotNull String userId, @NotNull String newEmail) throws Exception;

    void lockUserByLogin(@NotNull String login) throws Exception;

    void unlockUserByLogin(@NotNull String login) throws Exception;


}
