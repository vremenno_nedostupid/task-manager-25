package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.dto.UserDTO;
import ru.fedun.tm.entity.User;

public interface IUserEndpoint {

    void updateUserPassword(
            @NotNull SessionDTO session,
            @NotNull String password,
            @NotNull String newPassword
    ) throws Exception;

    void updateMail(@NotNull SessionDTO session, @NotNull String newEmail) throws Exception;

    @NotNull
    UserDTO showUserProfile(@NotNull SessionDTO session) throws Exception;

}
