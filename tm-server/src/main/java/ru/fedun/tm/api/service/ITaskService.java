package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.TaskDTO;
import ru.fedun.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    void createTask(@NotNull String userId, @NotNull String taskName, @NotNull String projectName, @NotNull String description) throws Exception;

    void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws Exception;

    void updateStartDate(@NotNull String userId, @NotNull String id, @NotNull Date date) throws Exception;

    void updateCompleteDate(@NotNull String userId, @NotNull String id, @NotNull Date date) throws Exception;

    @NotNull
    Long countAllTasks();

    @NotNull
    Long countByUserId(@NotNull String userId) throws Exception;

    @NotNull
    Long countByProjectId(@NotNull String projectId) throws Exception;

    @NotNull
    Long countByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull
    Task findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull
    Task findOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId) throws Exception;

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String projectId) throws Exception;

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    void removeOneById(@NotNull String userId, @NotNull String id) throws Exception;

    void removeOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    void removeAll();

    void removeAllByUserId(@NotNull String userId) throws Exception;

    void removeAllByProjectId(@NotNull String projectId) throws Exception;

    void removeAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

}
